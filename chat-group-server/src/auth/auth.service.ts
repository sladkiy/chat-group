import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { AuthSignInDto, AuthSignUpDto } from './dto/auth-credentials.dto';
import { JwtPayload } from './jwt.strategy';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signUp(authSignUpDto: AuthSignUpDto): Promise<User> {
    const user = await this.usersService.createUser(authSignUpDto);
    const payload: JwtPayload = { email: user.email, userId: String(user.id) };
    const accessToken = await this.jwtService.signAsync(payload);

    user.accessToken = accessToken;

    return user;
  }

  async signIn(authSignInDto: AuthSignInDto): Promise<User> {
    const user = await this.usersService.validateUserPassword(authSignInDto);

    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const payload: JwtPayload = { email: user.email, userId: String(user.id) };
    const accessToken = await this.jwtService.signAsync(payload);

    user.accessToken = accessToken;

    return user;
  }
}
