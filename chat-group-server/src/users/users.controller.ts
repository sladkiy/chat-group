import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './user.entity';
import { UsersService } from './users.service';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  // TODO: сделать авторизацию
  @Get()
  getUsers(): Promise<User[]> {
    return this.usersService.getUsers();
  }

  // TODO: сделать авторизацию
  @Post()
  @UsePipes(ValidationPipe)
  createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return this.usersService.createUser(createUserDto);
  }

  // TODO доделать редактирование формы
  @Post('/:id')
  @UseInterceptors(FileInterceptor('photo'))
  updateUserInfo(
    @UploadedFile() file: Express.Multer.File,
    @Body(new ValidationPipe({ whitelist: true })) updateUserDto: UpdateUserDto,
  ) {
    console.log(updateUserDto);
    console.log(file.destination);
  }
}
