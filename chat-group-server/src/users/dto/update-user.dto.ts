import {
  IsOptional,
  IsPhoneNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class UpdateUserDto {
  @IsOptional()
  @IsString()
  @MinLength(2)
  @MaxLength(50)
  name: string;

  @IsOptional()
  @IsString()
  @MinLength(10)
  @MaxLength(150)
  bio: string;

  @IsOptional()
  @IsPhoneNumber('RU')
  phone: string;
}
